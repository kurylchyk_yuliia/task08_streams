package com.learning_stream;
import org.apache.logging.log4j.*;

import java.util.Scanner;

public class View {

    private static Logger logger1 = LogManager.getLogger(View.class);
    View() { }

    public void showInfo(String str){
        logger1.info(str);
    }

    public String showMenu(){
        System.out.println("Select the option: ");
        for (Options op : Options.values()) {
            System.out.print(op + "\t");
        }
        Scanner sc = new Scanner(System.in);
        String option = sc.nextLine().toUpperCase();
        return option;
    }
}
