package com.learning_stream;

import sun.rmi.server.InactiveGroupException;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

public class MyStream {

    List<Integer> list;
    MyArray myArray;
    MyStream(){

        myArray = new MyArray();
        list = Arrays.asList(myArray.getArr5());
    }

    public Integer getMax() {

        Integer max = list.stream().mapToInt(v -> v)
                .max().orElseThrow(NoSuchElementException::new);
        return max;
    }

    public Integer getMin() {
        Integer min =list.stream().mapToInt(v->v).min().orElseThrow(NoSuchElementException::new);
        return min;
    }

    public String getAllElement(){
        Integer  arr[] = list.stream().toArray(Integer[]::new);

        String stringOfArray = "";
        for(int index = 0; index< arr.length; index++){
            stringOfArray+=arr[index]+" ";
        }
        return stringOfArray;
    }

    public Double getAverage(){

        Double average = list.stream().mapToInt(v->v).average().getAsDouble();

        return average;
    }

    public Integer getReduce(){
        Integer reduce  = list.stream().reduce(0,(i,j) -> {return i+j;});
        return reduce;
    }

    public Integer getSum(){
        Integer sum  = list.stream().mapToInt(v->v).sum();
        return sum;
    }

    public Long BiggerThanAverage() {

     Long biggerThanAverage = list.stream().filter(f -> f > getAverage()).count();
     return biggerThanAverage;
    }
}
