package com.learning_stream;

public class Controller {
    MyStream streamObject;
    View view = new View();

    Controller() {
        streamObject = new MyStream();
    }

    public void get(String option){

        Options op = Options.valueOf(option);
        switch (op){
            case SUM: view.showInfo("Sum of elements\t" + streamObject.getSum());
            break;
            case REDUCE:
                view.showInfo("Reduce of elements\t" + streamObject.getReduce());
                break;
            case ALL:
                view.showInfo("All the elements\t" + streamObject.getAllElement());
                break;
            case MAX:
                view.showInfo("Max value\t" + streamObject.getMax());
                break;
            case MIN:
                view.showInfo("Minvalue\t" + streamObject.getMin());
                break;
            case AVERAGE:
                view.showInfo("Average value\t" + streamObject.getAverage());
                break;
            case BIGGER_THAN_AVERAGE:
                view.showInfo("Bigger than average\t" + streamObject.BiggerThanAverage());
                break;
        }


    }



}
