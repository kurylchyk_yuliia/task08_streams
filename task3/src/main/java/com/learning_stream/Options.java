package com.learning_stream;

public enum Options {
    SUM,
    REDUCE,
    MIN,
    MAX,
    AVERAGE,
    ALL,
    BIGGER_THAN_AVERAGE
}
