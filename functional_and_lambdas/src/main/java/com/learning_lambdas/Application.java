package com.learning_lambdas;

public class Application {

    public static void main(String[] args) {
        SimpleMath maxValue = (int a,int b,int c)->  (a>b&&a>c)?a:((b>c &&b>c)?b:c);
        SimpleMath average = (int a, int b, int c) ->(a+b+c)/3;

        for(int index = 0; index<10; index++) {
            System.out.println("Max value " + maxValue.operationWithInt(index+1,index+4, index-8));
            System.out.println("Average "+average.operationWithInt(index-1,index+7,index-2));
        }


    }
}
