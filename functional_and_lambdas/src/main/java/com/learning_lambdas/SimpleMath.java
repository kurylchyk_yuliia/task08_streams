package com.learning_lambdas;

@FunctionalInterface
public interface SimpleMath {
     int operationWithInt(int a, int b, int c);
}
