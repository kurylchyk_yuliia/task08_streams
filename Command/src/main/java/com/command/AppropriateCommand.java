package com.command;

/**
 * Helps to  call the appropriate methods
 */
public enum AppropriateCommand {
  LAMBDA, METHODREF, ANONCLASS, CLASS
}
