package com.command;

public interface Command {
   void action(String str);
}
