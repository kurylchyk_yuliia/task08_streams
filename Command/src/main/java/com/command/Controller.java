package com.command;
import java.util.Scanner;

/**
 * Class which operates with expressions
 */
public class Controller {

    String str;
    /**
     * Ask user to enter the str
     */
    Controller() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the string : ");
        str = sc.nextLine();
        call(setTheCommand(), str);
    }


    /**
     * Allows user to choose the command
     * @return the appropriate command
     */
    public String setTheCommand() {

        String command;
        Scanner sc = new Scanner(System.in);
        for (AppropriateCommand ac : AppropriateCommand.values()) {
            System.out.print(ac + "\t");
        }
        System.out.println("\nChoose the command");
        command = sc.nextLine().toString();
        return command;
    }

    /**
     * Calls appropriate command with different expressions
     * @param chosenOption
     * @param userInput
     */
    public void call(String chosenOption, String userInput) {

        AppropriateCommand ap = AppropriateCommand.valueOf(chosenOption);

        switch (ap) {

            case LAMBDA:

                 Command lam = (str)->{ System.out.println(str+ "\tHello"); };
                 lam.action(userInput);
                break;

            case METHODREF:
                 Command commandImpl = new CommandImpl();
                 Command methodRef = commandImpl::action;
                 methodRef.action(userInput);
                break;
            case ANONCLASS:
                Command anon = new Command() {
                    @Override
                    public void action(String str) {

                        System.out.println(str + " I am an anon class");
                    }
                };
                anon.action(userInput);
                break;

            case CLASS:
                CommandImpl command = new CommandImpl();
                command.action(userInput);
                break;
        }

    }
}
