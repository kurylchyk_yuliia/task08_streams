package com.learning_streams;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Ask user to input the data
 */
public class Input {
    /**
     * list for storing data
     */
    List<String> listOfStr;
    private Scanner sc;

    /**
     * Initialize the variables and gets user's input
     */
    Input(){
        listOfStr= new ArrayList<String>();
        sc = new Scanner(System.in);
         inputLines();
    }

    private void inputLines() {
       String str;
        do{
            System.out.print("Enter the line: ");
            str = sc.nextLine();
           if(str.isEmpty()) {
               break;
           }
            listOfStr.add(str);
        }while(true);

    }

    /**
     *
     * @return the data
     */
    public List<String> getList() {
        return listOfStr;
    }


}
