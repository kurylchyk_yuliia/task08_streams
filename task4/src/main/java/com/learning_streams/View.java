package com.learning_streams;

/**
 * used for showing info
 */
@FunctionalInterface
public interface View {
    void ShowInfo(String info);
}
