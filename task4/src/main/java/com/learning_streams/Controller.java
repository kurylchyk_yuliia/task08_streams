package com.learning_streams;
import org.apache.logging.log4j.*;

import java.util.Scanner;

/**
 * class which operates Stream and User input
 */
public class Controller {


    Logger logger1 = LogManager.getLogger(MyStream.class);
    MyStream myStream;

    /**
     * Initialize the variables and calls the methods
     */
    Controller(){
        myStream = new MyStream();
        myStream.countTheSameWord();

        do {
            String option = getOption();
            if(option=="Q"){
                System.exit(0);
            }
            getMethod(option);
        }while(true);
    }


    /**
     * Ask user to choose the option
     * @return the option
     */
    private String getOption(){

        Scanner sc = new Scanner(System.in);
        String chosenOption;
        for(Option op: Option.values()){
            System.out.print(op+"\t");
        }
        System.out.print("\tQ-exit");
        System.out.print("\nChoose the option: ");
        chosenOption= sc.nextLine().toUpperCase();
       return chosenOption;

    }

    /**
     * Calls appropriate method using Lambda
     * @param option
     */
    public void getMethod(String option){

        Option op = Option.valueOf(option);
        View lam = (str) ->logger1.info(str);
        switch(op){

            case NUMBER_OF_UNIQUE:
                Long numOfUnique = myStream.getNumberOfUnique();

                lam.ShowInfo("Number of unique " + numOfUnique);
                break;
            case SORTED_LIST_OF_UNIQUE:
                String sortedList  = myStream.sortedListOfUnique().toString();
                lam.ShowInfo("Sorted list:"+sortedList);
                break;
            case OCCURRENCE_OF_WORDS:
                String occurrenceOfWords =myStream.countTheSameWord().toString();
                lam.ShowInfo("The occurrence of word :\n" + occurrenceOfWords);
                break;
            case OCCURRENCE_OF_UPPERCASE:
                Long upperCase = myStream.getCountOfUpperCase();
                lam.ShowInfo("Occurrence of upper case symbols: " + upperCase);
                break;
        }
    }

}
