package com.learning_streams;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * class which operates with stream
 */
public class MyStream {
    private Input input;
    private List<String> userInput;


    /**
     * initialize the variables and split them.
     */
    MyStream() {
        input = new Input();
        userInput = input.getList();
        splitString();
    }

    /**
     * splits the spring
     */
    private void splitString(){

        List<String> list = userInput.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .collect(Collectors.toList());
        userInput = list;

    }

    /**
     * finds the number of unique words
     * @return count of unique words
     */
    public Long getNumberOfUnique(){
        Long unique = userInput.stream().distinct().count();
        return unique;
    }

    /**
     * sort the  list
     * @return sorted list
     */
    public List<String> sortedListOfUnique(){
        Stream<String> sorted = userInput.stream().distinct().sorted();
        List<String> sortedList = sorted.collect(Collectors.toList());
        return sortedList;

    }

    /**
     * counts the words in list
     * @return map of counted words
     */
    public Map<String,Long> countTheSameWord(){

        Map<String, Long> counted = userInput.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        return counted;
    }

    /**
     * counts the number of upper case symbol in stream
     * @return the count of  upper case symbol
     */
    public Long getCountOfUpperCase(){

        String upperCase = userInput.toString();
        Long upperCaseSymbol = upperCase.chars().filter((c) -> Character.isUpperCase(c)).count();
        return upperCaseSymbol;
    }

}
