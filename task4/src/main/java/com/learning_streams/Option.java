package com.learning_streams;

/**
 * used for choosing the appropriate method
 */
public enum Option {

    NUMBER_OF_UNIQUE,
    SORTED_LIST_OF_UNIQUE,
    OCCURRENCE_OF_WORDS,
    OCCURRENCE_OF_UPPERCASE
}
